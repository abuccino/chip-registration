from scipy.io import loadmat
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
import pickle
import tifffile

this_file_path = Path(__file__)
img_folder = this_file_path.parent.parent / "img"


def mea1k_registration_instructions():
    import matplotlib.pyplot as plt
    import matplotlib.image as mpimg

    mesg = """Move the microscope stage and note down the x, y positions of channels 0, 219, and 26399 (as shown
in the image. Then instantiate the Mea1kRegistration object:\n\n

import chip_registration as ct\n\n

mea1kr = cr.Mea1kRegistration(ch0_position, ch219_position, ch26399_position)

registered_positions = mea1kr.corrected_locations\n\n"""

    print(mesg)
    image = mpimg.imread(img_folder / "mea1k.png")
    plt.imshow(image)
    plt.axis("off")


def dualmode_registration_instructions():
    import matplotlib.pyplot as plt
    import matplotlib.image as mpimg

    mesg = """Move the microscope stage and note down the x, y positions of channels 1, 9944, and 20736 (as shown
in the image. Then instantiate the DualModeRegistration object:\n\n

import chip_registration as ct\n\n

dmr = cr.DualModeRegistration(ch1_position, ch9944_position, ch20736_position)

registered_positions = dmr.corrected_locations\n\n"""

    print(mesg)
    image = mpimg.imread(img_folder / "dualmode.png")
    plt.imshow(image)
    plt.axis("off")


def save_to_pickle(reg_obj, save_path):
    save_path = Path(save_path)
    assert save_path.suffix in [".pkl", ".pickle"], "The 'save_path' should be a pickle file!"

    dump_dict = {}
    dump_dict["class"] = type(reg_obj)
    dump_dict["kwargs"] = reg_obj._kwargs

    with save_path.open("wb") as f:
        pickle.dump(dump_dict, f)

def load_from_pickle(load_path):
    load_path = Path(load_path)
    assert load_path.suffix in [".pkl", ".pickle"], "The 'save_path' should be a pickle file!"

    with load_path.open("rb") as f:
        dump_dict = pickle.load(f)

    cls = dump_dict['class']
    # cls = _get_class_from_string(class_name)
    kwargs = dump_dict["kwargs"]

    return cls(**kwargs)


def overlay_image(image, stage_position=None, pixel_per_um=1, top_left=None, bottom_right=None, cmap='Greys',
                  corrections=[0, 0], ax=None):
    """

    Parameters
    ----------
    image: str, Path, or np.array
        Image to be displayed. If str or Path: path to tif file, if array: numpy array with loaded image
    stage_position: list
        x, y stage position of the image
    pixel_per_um: float
        Number of pixels per um
    top_left: list
        x, y stage position of the top left when set by stage nd-image
    bottom_right: list
        x, y stage position of the bottom left when set by stage nd-image
    cmap: matplotlib colormap
        The colormap to be used (default Greys)
    corrections: list
        Allows to correct for systematic errors in x and y directions
    ax: matplotlib axes

    Returns
    -------
    ax: the axes with the image
    im: the image mappable (which can be used for colorbar)
    """
    if isinstance(image, (str, Path)):
        image = tifffile.imread(image)

    if ax is None:
        fig, ax = plt.subplots()

    plt.set_cmap(cmap)
    image_size = np.array(image.T.shape)

    if stage_position is not None:
        stage_position = np.array(stage_position)
        print(f"Image size in um {(image_size * pixel_per_um)}")

        bottom_left = stage_position - (image_size * pixel_per_um) // 2
        top_right = stage_position + (image_size * pixel_per_um) // 2
        bottom_left += corrections
        top_right += corrections  # [-20, 35]
        left, bottom = bottom_left
        right, top = top_right

        extent = (left, right, bottom, top)
    else:
        assert top_left is not None and bottom_right is not None
        top_left = top_left - (np.array([1024, 1024]) * pixel_per_um) // 2
        bottom_right = bottom_right + (np.array([1024, 1024]) * pixel_per_um) // 2
        extent = (top_left[0], bottom_right[0], top_left[1], bottom_right[1])

    im = ax.imshow(image, alpha=0.9, origin='lower',  # vmin=0, vmax=1000,
                   extent=extent)

    return ax, im

def parse_dualmode_locations_electrodes(mat_params_file):
    info = loadmat(mat_params_file)
    ntk = info['ntk']
    fs = _squeeze_ds(ntk['sr'])
    lsb = _squeeze_ds(ntk['lsb'])
    x = ntk['x2'][0][0][0]
    y = ntk['y2'][0][0][0]
    channel_ids = ntk['channel_nr'][0][0][0].astype('int')
    electrode_ids = ntk['el_idx2'][0][0][0].astype('int')
    locations = np.array([x, y]).T

    return locations, electrode_ids


def _squeeze_ds(ds):
    while not isinstance(ds, (int, float, np.integer, np.float)):
        ds = ds[0]
    return ds

#
# def _get_class_from_string(class_string):
#     class_name = class_string.split('.')[-1]
#     module = '.'.join(class_string.split('.')[:-1])
#     imported_module = importlib.import_module(module)
#
#     try:
#         imported_class = getattr(imported_module, class_name)
#     except:
#         imported_class = None
#
#     return imported_class
