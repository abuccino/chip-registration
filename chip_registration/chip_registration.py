import numpy as np
from pathlib import Path
import MEAutility as mu
from copy import deepcopy
import pickle

from .utils import parse_dualmode_locations_electrodes

this_file_path = Path(__file__)
this_parent_path = this_file_path.parent
matlab_params_file = this_parent_path / 'aps_array.mat'


class DualModeRegistration:
    """
    Class to register DualMode HD-MEA to microscopy stages.

    Parameters
    ----------

    ch_1_stage_position: array-like
        Array with 2D stage position for channel 1
    ch_9944_stage_position: array-like
        Array with 2D stage position for channel 9944
    ch_20746_stage_position: array-like
        Array with 2D stage position for channel 20746
    mirror_y: bool
        If True, y positions are mirrored (default True).
        This means that channel 1 is "above" channels 9944 and 20746
    mirror_x: bool
        If True, x positions are mirrored (default False).
        This means that channel 1 is "at the left" of channel 20746
    """

    def __init__(self, ch_1_stage_position, ch_9944_stage_position, ch_20746_stage_position):
        loc1 = np.array([0, 0])
        loc9944 = np.array(ch_9944_stage_position) - np.array(ch_1_stage_position)
        loc20746 = np.array(ch_20746_stage_position) - np.array(ch_1_stage_position)

        self._kwargs = {"ch_1_stage_position": ch_1_stage_position,
                        "ch_9944_stage_position": ch_9944_stage_position,
                        "ch_20746_stage_position": ch_20746_stage_position}

        self.ch_1_stage_position = ch_1_stage_position
        self.ch_9944_stage_position = ch_9944_stage_position
        self.ch_20746_stage_position = ch_20746_stage_position

        all_locations, all_electrodes = parse_dualmode_locations_electrodes(matlab_params_file)
        all_channels = np.arange(len(all_locations))

        self.all_locations = all_locations - all_locations[0]
        self.all_channels = all_channels
        self.all_electrodes = all_electrodes

        self.dm = mu.return_mea(info={'pos': all_locations, 'center': False, 'plane': 'xy'})

        if np.cross(loc20746 - loc9944, loc9944) < 0:
            mirror_x = False
        else:
            mirror_x = True

        if mirror_x:
            self.dm.rotate([0, 1, 0], 180)

        all_locations = self.dm.positions[:, :2] - self.dm.positions[0, :2]

        # compute theta
        l = np.linalg.norm(all_locations[9943] - all_locations[0])
        l_tilde = np.linalg.norm(loc9944 - loc1)
        theta = np.rad2deg(np.arccos(l_tilde / l))
        if np.isnan(theta):
            print(f"Extracted side length is longer than theoretical one by {np.abs(l - l_tilde)}. Setting "
                  f"theta to 0")
            theta = 0
        print(f"Theta: {theta}")

        # compute phy
        v_tilde = loc20746 - loc1
        v = all_locations[-1] - all_locations[0]
        phi = np.rad2deg(np.arccos((np.dot(v, v_tilde)) / (np.linalg.norm(v) * np.linalg.norm(v_tilde))))
        print(f"Phi: {phi}")

        # apply rotations
        self.dm.rotate([1, 0, 0], theta)
        if np.cross(v, v_tilde) > 0:
            self.dm.rotate([0, 0, 1], phi)
            self.phi = phi
        else:
            self.dm.rotate([0, 0, 1], -phi)
            self.phi = -phi

        self.theta = theta

        self.corrected_locations = self.dm.positions[:, :2]
        self.corrected_locations -= self.corrected_locations[0]
        self.corrected_locations += ch_1_stage_position

    @staticmethod
    def get_all_locations():
        all_locations, all_electrodes = parse_dualmode_locations_electrodes(matlab_params_file)
        return deepcopy(all_locations)

    @staticmethod
    def get_all_channels_ids():
        all_locations, _ = parse_dualmode_locations_electrodes(matlab_params_file)
        all_channels = np.arange(len(all_locations))
        return deepcopy(all_channels)

    @staticmethod
    def get_all_electrode_ids():
        _, all_electrodes = parse_dualmode_locations_electrodes(matlab_params_file)
        return deepcopy(all_electrodes)

    def _apply_transform(self, locations):
        mea = mu.return_mea(info={'pos': locations, 'center': False, 'plane': 'xy'})
        # apply rotations
        mea.rotate([1, 0, 0], self.theta)
        mea.rotate([0, 0, 1], self.phi)

        corrected_locations = mea.positions[:, :2]
        corrected_locations -= corrected_locations[0]
        corrected_locations += self.ch_1_stage_position
        return corrected_locations

    def get_stage_position_for_channel(self, channel_id):
        assert channel_id in self.all_channels
        stage_x, stage_y = self.corrected_locations[list(self.all_channels).index(channel_id)]
        print(f"x stage position: {stage_x}\ny stage position: {stage_y}")
        return [stage_x, stage_y]

    def get_channel_for_stage_position(self, stage_x, stage_y):
        dists = []
        for loc in self.corrected_locations:
            dist = np.linalg.norm(loc - np.array([stage_x, stage_y]))
            dists.append(dist)
        return self.all_channels[np.argmin(dists)]

    def get_stage_position_for_electrode(self, electrode_id):
        assert electrode_id in self.all_electrodes
        stage_x, stage_y = self.corrected_locations[list(self.all_electrodes).index(electrode_id)]
        print(f"x stage position: {stage_x}\ny stage position: {stage_y}")
        return [stage_x, stage_y]

    def get_electrode_for_stage_position(self, stage_x, stage_y):
        dists = []
        for loc in self.corrected_locations:
            dist = np.linalg.norm(loc - np.array([stage_x, stage_y]))
            dists.append(dist)
        return np.array(self.all_electrodes)[np.argmin(dists)]


class Mea1kRegistration:
    """
    Class to register Mea1k HD-MEA to microscopy stages.

    Parameters
    ----------

    el_0_stage_position: array-like
        Array with 2D stage position for channel 0
    el_219_stage_position: array-like
        Array with 2D stage position for channel 219
    el_26399_stage_position: array-like
        Array with 2D stage position for channel 26180
    """

    def __init__(self, el_0_stage_position, el_219_stage_position, el_26399_stage_position):
        loc0 = np.array([0, 0])
        loc219 = np.array(el_219_stage_position) - np.array(el_0_stage_position)
        loc26399 = np.array(el_26399_stage_position) - np.array(el_0_stage_position)

        self._kwargs = {"el_0_stage_position": el_0_stage_position,
                        "el_219_stage_position": el_219_stage_position,
                        "el_26399_stage_position": el_26399_stage_position}

        self.el_0_stage_position = el_0_stage_position
        self.el_219_stage_position = el_219_stage_position
        self.el_26399_stage_position = el_26399_stage_position

        all_locations = get_mea1k_all_locations()

        self.all_locations = all_locations - all_locations[0]
        self.all_electrodes = get_mea1k_all_electrode_ids()

        self.mea1k = mu.return_mea(info={'pos': all_locations, 'center': False, 'plane': 'xy'})

        if np.cross(loc219, loc26399 - loc219) >= 0:
            mirror_x = False
        else:
            mirror_x = True

        if mirror_x:
            self.mea1k.rotate([0, 1, 0], 180)

        all_locations = self.mea1k.positions[:, :2] - self.mea1k.positions[0, :2]

        # compute theta
        l = np.linalg.norm(all_locations[219] - all_locations[0])
        l_tilde = np.linalg.norm(loc219 - loc0)
        theta = np.rad2deg(np.arccos(l_tilde / l))
        if np.isnan(theta):
            print(f"Extracted side length is longer than theoretical one by {np.abs(l - l_tilde)}. Setting "
                  f"theta to 0")
            theta = 0
        print(f"Theta: {theta}")
        self.theta = theta

        # compute phy
        v_tilde = loc26399 - loc0
        v = all_locations[-1] - all_locations[0]
        phi = np.rad2deg(np.arccos((np.dot(v, v_tilde)) / (np.linalg.norm(v) * np.linalg.norm(v_tilde))))
        print(f"Phi: {phi}")
        self.phi = phi

        # apply rotations
        self.mea1k.rotate([1, 0, 0], theta)
        if np.cross(v, v_tilde) > 0:
            self.mea1k.rotate([0, 0, 1], phi)
        else:
            self.mea1k.rotate([0, 0, 1], -phi)

        self.corrected_locations = self.mea1k.positions[:, :2]
        self.corrected_locations -= self.corrected_locations[0]
        self.corrected_locations += el_0_stage_position

    def _apply_transform(self, locations):
        mea = mu.return_mea(info={'pos': locations, 'center': False, 'plane': 'xy'})
        # apply rotations
        mea.rotate([1, 0, 0], self.theta)
        mea.rotate([0, 0, 1], self.phi)

        corrected_locations = mea.positions[:, :2]
        corrected_locations -= corrected_locations[0]
        corrected_locations += self.corrected_locations[0]
        return corrected_locations

    @staticmethod
    def get_all_locations():
        return deepcopy(get_mea1k_all_locations())

    @staticmethod
    def get_all_electrode_ids():
        return deepcopy(get_mea1k_all_electrode_ids())

    def get_stage_position_for_electrode(self, electrode_id):
        assert electrode_id in self.all_electrodes
        stage_x, stage_y = self.corrected_locations[self.all_electrodes.index(electrode_id)]
        return [stage_x, stage_y]

    def get_electrode_for_stage_position(self, stage_x, stage_y):
        dists = []
        for loc in self.corrected_locations:
            dist = np.linalg.norm(loc - np.array([stage_x, stage_y]))
            dists.append(dist)
        return np.array(self.all_electrodes)[np.argmin(dists)]


def get_mea1k_all_locations():
    electrode_ids = get_mea1k_all_electrode_ids()
    locations = np.zeros((len(electrode_ids), 2))

    pitch_x = 17.5
    pitch_y = 17.5

    for el in electrode_ids:
        locations[el, 0] = np.mod(el, 220) * pitch_x
        locations[el, 1] = (el // 220) * pitch_y

    return locations


def get_mea1k_all_electrode_ids():
    return list(range(26400))
