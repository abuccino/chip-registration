# chip-registration

Python package to register Mea1k, Dualmode, and NeuroCMOS chips to microscope stages.

## Installation

Clone the repository and install as follows:

```bash
git clone https://git.bsse.ethz.ch/abuccino/chip-registration.git
cd chip-registration 
pip install .
```

## Usage

From Python, you can show the registration procedure for the Mea1k and DualMode chips:

```python
import chip_registration as cr

cr.mea1k_registration_instructions()

cr.dualmode_registration_instructions()
```

Once an chip registration object is instantiated, it provides useful functions to transform form and to the stage and
the chip coordinates

```python
mea1kreg = cr.Mea1kRegistration(ch0_position, ch219_position, ch26399_position)

el_id = mea1kreg.get_electrode_for_stage_position([2000, 15000])
stage_x, stage_y = mea1kreg.get_stage_position_for_electrode(10000)
```

Registration object can be conveniently saved to pickle and reloaded with:

```python
cr.save_to_pickle("chip-registration-210208.pkl")

mea1kreg_loaded = cr.load_from_pickle("chip-registration-210208.pkl")
```

Finally, the `overlay_image()` function allows one to display a tif image at the right stage position (note down 
the `picel_per_um` based on the microscope objective!):

```python
cr.overlay_image("my-cell1.tif", stage_position=[2000, 15000], pixel_per_um=0.21090)
```
