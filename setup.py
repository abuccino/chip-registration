# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

d = {}
exec(open("chip_registration/version.py").read(), None, d)
version = d['version']
long_description = open("README.md").read()

entry_points = None

install_requires = []

setup(
    name="chip_registration",
    version=version,
    author="Alessio Buccino",
    author_email="alessiop.buccino@gmail.com",
    description="Python package for registration of BEL chips to microscope stages",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.bsse.ethz.ch/abuccino/chip-registration",
    install_requires=[
        'numpy',
        'matplotlib',
        'MEAutility',
        'tifffile'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(),
    entry_points=entry_points,
    include_package_data=True,
)
